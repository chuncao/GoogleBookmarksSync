git用途：此处git主要为了同步谷歌书签使用

谷歌书签文件：C:\Users\Administrator\AppData\Local\Google\Chrome\User Data\Profile 3\Bookmarks

此处目前主要包括如下几个主要文件
	Bookmarks			谷歌书签文件
	.gitignore			控制包含在git的文件
	sync-command.bat	window脚本文件
	Readme.txt			说明文件
	window-cron.xml		window定时xml，可直接导入【任务计划程序】，git每天0点开始，每15分钟提交一次谷歌书签

注意：
建议将脚本放入谷歌书签所在的目录下


相关文档参考：
window脚本设置方法：https://blog.csdn.net/u013788943/article/details/81629645